


describe('login client', () => {

    beforeEach(() => {
  
            cy.visit ("https://dev.app.easyadvo.devateam.com/authentication/login")
            //cy.title().should('eq', 'Anmelden|EasyAdvo')
       // cy.get(".mat-mdc-card-actions > .btn-primary-blue").click()             // cookies accept

        cy.contains('Anmelden').should('exist');    
            cy.get(":nth-child(1) > .form-control").type("Mohind@gmail.com")
            cy.get(".password > .form-control").type("Mohind@123")
            cy.get('.btn[type="submit"]').click()
            cy.wait(3000);


            cy.visit("https://dev.app.easyadvo.devateam.com/client/home/advisor-search")

    })

    /* it('Client Profile', () => {

        //cy.get(".mat-mdc-card-actions > .btn-primary-blue").click()             // cookies accept

        cy. visit("https://dev.app.easyadvo.devateam.com/client/profile")
        cy.contains('Mein Profil').should ('exist')

        cy.wait(3000)

        cy.get(".edit-profile").click()


        cy.contains('Profil bearbeiten').should ('exist')

        cy.get('Profil').should('not.exist')

        cy.get(':nth-child(1) > :nth-child(1) > .form-group > .form-control').clear().type("Mohindtest")   //edit firt name
        cy.get(':nth-child(1) > :nth-child(2) > .form-group > .form-control').clear().type("one")    //edit second name

        cy.get(':nth-child(3) > .form-control').clear().type("+3278(738)343()334")   // edit phone number

    cy.get(':nth-child(4) > :nth-child(2) > .form-group > .form-control').clear().type(12348)   //edit postal code


        //cy.get('.btn-primary-blue').then(($button) => {
           // const $parentDiv = $button.parent(); // or .closest('div')

      // Assert that the parent div is found
      //expect($parentDiv).to.exist

cy.get('.modal-footer > .d-flex > .btn-primary-blue').click()

        cy.wait(2000)

        cy.get('.ng-trigger > .ng-tns-c7-7').should('be.visible');
        cy.wait(2000)

        

    }) */




   /* it('Advisor Search and Profile view',() => {
        //cy.visit("https://dev.app.easyadvo.devateam.com/client/home/advisor-search")

     // cy.get(".mat-mdc-card-actions > .btn-primary-blue").click()             // cookies accept
        cy.contains('Finde deinen Anwalt').should('exist')

        cy.contains('Suchergebnisse').should ('exist')

        cy.wait(3000)
        cy.get('.d-flex > .location').select('Berlin')
        cy.get('[aria-label="topic"]').select('Civil law')

        cy.get('.search-btn').click()
        cy.wait (3000);

        cy.get('.search-list-md > .search-list-item').should('be.visible');
        cy.wait(3000)

        cy.get('.btn-link').click()  //advior profile view
        cy.get(':nth-child(2) > .flex-sm-nowrap > .justify-content-sm-end > .btn-box > .btn-primary-blue').click() 
        cy.wait(3000)

        //cy.contains('Profil').should('exists')
        //cy.contains ('Beratungen').should ('exists')
        //cy.contains ('Erfahrungen').should ('exists')

        //cy.contains('Profil bearbeiten').should ('not.exist')    // negative assertion

        cy.get('.arrow-left').click()

        cy.wait(3000)

        cy.get('.btn-link').click()  //advior session detail view
        cy.wait(3000)

        cy.contains('Anfrage erstellen').should('exists')
        cy.contains ('Falldetails').should ('exists')

       // cy.contains('Profil bearbeiten').should ('not.exist')

    
        cy.get('.btn-link > .text-14').click()

        cy.wait(2000)  


        //cy.get('.d-flex > .location').select('Cologne')
       // cy.get('[aria-label="topic"]').select('Civil Procedure')

        
       //cy.get('.search-btn').click()
       // cy.contains("Suchergebnisse(0)").should('exists')
        //cy.wait (2000);
   }) */


       it('Advisor Search and Request session',() => {
 
           // cy.get(".mat-mdc-card-actions > .btn-primary-blue").click()             // cookies accept
        cy.contains('Finde deinen Anwalt').should('exist')

        cy.contains('Suchergebnisse').should ('exist')

        cy.wait(3000)
        
        cy.get('.d-flex > .location').select('Berlin')
        cy.get('[aria-label="topic"]').select('Family law')

        cy.get('.search-btn').click()
        cy.wait (1000);

        cy.get('.search-list-md > .search-list-item').should('be.visible');
        cy.wait(2000)

       cy.get('.search-list-md').scrollTo('bottom')
       cy.wait(2000)

       cy.get('.search-list-md').scrollTo('top')

       cy.wait(2000)
cy.get('.search-list-md > :nth-child(1) > .btn-box > .btn-primary-blue').click()

        //cy.get('.btn-primary-blue').click()    //click on create request to show validation

        cy.wait(2000)

        cy.get('.info > .btn-box > .btn-primary-blue').click()
        cy.contains('Falldetails sind notwendig ').should('exist')
        cy.wait(1000)

       // cy.get('.mb-0').scrollTo('bottom')
        cy.wait(1000)

        cy.get('#details').click()
      
       

cy.wait(1000)
            cy.get('.mat-mdc-tooltip-trigger')           //mouse hover on tooltip
            .realHover();

    
      cy.contains('Dokumente hochladen');
    

            cy.wait(3000)

            cy.get('.mat-mdc-tooltip-trigger') 
            .trigger('mouseout');
      
          
          cy.contains('Dokumente hochladen').should('not.be.visible');

           // cy.get('#details').type("New cypress code uploaded image and files")

        

           /* cy.get('.mat-mdc-tooltip-trigger').selectFile("C:\\Users\\hp\\Downloads\\blog-image01 (1).jpg", {force: true})    //1st file
            .selectFile("C:\\Users\\hp\\Downloads\\report(50).pdf", {force: true})                     //2nd
            .selectFile("C:\\Users\\hp\\Downloads\\FAQ EasyAdvo.docx", {force: true}) */             //3rd

          

            //cy.contains(" Ungültig Dateityp Nur JPEG, PNG, JPG, PDF, DOC, und DOCX ").should('exist')    //validate error message

        
           //.selectFile("C:\\Users\\hp\\Downloads\\SamplePNGImage_20mbmb.png", {force: true})    //more than 8mb

           //cy.contains('Dateigröße darf nicht mehr als 8 MB sein ').should('exist')        //va;idate file size validation
           
//.selectFile("blog-image02 (1).jpg",{force: true})
cy.get('.mat-mdc-tooltip-trigger').should('exist');

const filePath = "report.pdf"
const filePath1 = "new.jpeg"
const filePath2 = "SampleJPGImage_10mbmb.jpg"

cy.get('input[type="file"]').attachFile(filePath);
cy.get('input[type="file"]').attachFile(filePath1);
cy.get('input[type="file"]').attachFile(filePath2);

cy.contains('Dateigröße darf nicht mehr als 8 MB sein ').should('exist') 
cy.wait(1000)

const filePath3 = "export-2023-12-15_06_21_16.xls"
cy.get('input[type="file"]').attachFile(filePath3);
cy.contains('Nur JPEG, PNG, JPG, PDF, DOC, und DOCX ').should('exist')
cy.wait(1000)

const filePath4 = "blog-image02 (1).jpg"
cy.get('input[type="file"]').attachFile(filePath4);

const filePath5 = "report(6).pdf"
cy.get('input[type="file"]').attachFile(filePath5);

const filePath6 = "new.jpeg"
cy.get('input[type="file"]').attachFile(filePath6);


const filePath7 = "blog-image02 (1).jpg"                         // 5 document limit exceeded
cy.get('input[type="file"]').attachFile(filePath7);

cy.contains('Bis zu 5 Dateien hochladen ').should('exist')
cy.wait(1000)



            cy.get('.info > .btn-box > .btn-primary-blue').click() //submit request button
            cy.wait(1000)

           // cy.get('.ng-trigger > .ng-tns-c7-9').should('be.visible');

         

            cy.wait(10000)

            //cy.visit("https://dev.app.easyadvo.devateam.com/client/requests/list")

          //  cy.wait(2000)
             



     
    })
})
