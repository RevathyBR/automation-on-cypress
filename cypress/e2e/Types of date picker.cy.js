describe('Date picker', () => {

    it('Drop down date picker', () => {
        cy.visit('https://opensource-demo.orangehrmlive.com/web/index.php/auth/login')
        cy.get('.oxd-text--h5').should('exist')
        cy.get('.orangehrm-login-branding > img').should('exist');

        cy.get('input[data-v-1f99f73c].oxd-input.oxd-input--active[name="username"][placeholder="Username"][autofocus]').type('Admin')
        .should('exist')
        cy.get(':nth-child(3) > .oxd-input-group > :nth-child(2) > .oxd-input').type('admin123')
        .should('be.visible')
      
        cy.get('.oxd-button').click();

        cy.visit('https://opensource-demo.orangehrmlive.com/web/index.php/leave/viewLeaveList')
      

        cy.get(':nth-child(1) > .oxd-input-group > :nth-child(2) > .oxd-date-wrapper > .oxd-date-input > .oxd-icon').click()
        cy.get(':nth-child(1) > .oxd-input-group > :nth-child(2) > .oxd-date-wrapper > .oxd-date-input > .oxd-icon').should('be.visible')
        cy.get('select').select('2024-05-01').should('have.value', '2024-05-01')


       // cy.get('.right > .calendar-table > .table-condensed > tbody > :nth-child(2) > [data-title="r1c1"]').click();
       // cy.get('.right > .calendar-table > .table-condensed > tbody > :nth-child(2) > [data-title="r1c5"]').click();
})
})