describe('Date picker', () => {

    beforeEach(() => {
  
        cy.visit ("https://dev.app.easyadvo.devateam.com/authentication/login")
       // cy.title().should('eq','EasyAdvo')
    cy.get(".mat-mdc-card-actions > .btn-primary-blue").click()             // cookies accept

    cy.contains('Anmelden').should('exist');  
    
   

        cy.get(":nth-child(1) > .form-control").type("sinu@gmail.com")
        cy.get(".password > .form-control").type("Sinu@12345")
        cy.get('.btn[type="submit"]').click()
        cy.wait(3000);


    })

        
    it(' Experience ', () => {

        cy.visit("https://dev.app.easyadvo.devateam.com/advisor/profile")
        //cy.url().should("https://dev.app.easyadvo.devateam.com/advisor/profile")
        //.and('contain','advisor profile')

   // cy.title().should('eq','Anwalt Profil')
   cy.contains('Erfahrungen').should('exist')
   cy.get('.edit-experience > img').click()

   cy.contains('Erfahrung hinzufügen').should('exist')
   cy.screenshot('homepage')

   cy.get(':nth-child(1) > .form-group > .input-with-icon > .mat-mdc-form-field > .mat-mdc-text-field-wrapper > .mat-mdc-form-field-flex > .mat-mdc-form-field-icon-suffix > .mat-datepicker-toggle > .mdc-icon-button > .mat-mdc-button-touch-target').click()
   //cy.get(':nth-child(1) > .form-group > .input-with-icon > .mat-mdc-form-field > .mat-mdc-text-field-wrapper > .mat-mdc-form-field-flex > .mat-mdc-form-field-icon-suffix > .mat-datepicker-toggle > .mdc-icon-button > .mat-mdc-button-touch-target').type('2022-12-20');
cy.get(':nth-child(3) > [data-mat-col="1"] > .mat-calendar-body-cell').click()

/*cy.get('.mat-calendar-previous-button > .mat-mdc-focus-indicator').click()
cy.get('.mdc-button__label > span').select('2023')
cy.get(':nth-child(4) > [data-mat-col="3"] > .mat-calendar-body-cell').click()

   cy.get('#calendar .day[data-date="2023-05-20"]').click();

    cy.get('.datepicker-year-select').contains('2020').click(); // Example for selecting the year
    
   cy.get('.datepicker-month-select').contains('May').click(); // Example for selecting the month

    // Select the day
    cy.get('.datepicker-day').contains('1').click(); // This is highly dependent on the date picker's structure

    // Verify the input field contains the correct date
    cy.get(':nth-child(1) > .form-group > .input-with-icon > .mat-mdc-form-field > .mat-mdc-text-field-wrapper > .mat-mdc-form-field-flex > .mat-mdc-form-field-icon-suffix > .mat-datepicker-toggle > .mdc-icon-button > .mat-mdc-button-touch-target').should('have.value', '2020-05-01');
    })*/

    cy.get(':nth-child(2) > .form-group > .input-with-icon > .mat-mdc-form-field > .mat-mdc-text-field-wrapper > .mat-mdc-form-field-flex > .mat-mdc-form-field-icon-suffix > .mat-datepicker-toggle > .mdc-icon-button > .mat-mdc-button-touch-target').click()

    const targetDate = new Date(2023, 11, 25) // Months are 0-indexed, so 11 is December

    // Use a library like `date-fns` or `moment` for easier date manipulation if needed
    const targetYear = targetDate.getFullYear()
    const targetMonth = targetDate.toLocaleString('default', { month: 'long' })
    const targetDay = targetDate.getDate()

    cy.get('.datepicker-year').contains(targetYear).click()

    // Navigate to the desired month
    cy.get('.datepicker-month').contains(targetMonth).click()

    // Select the desired day
    cy.get('.datepicker-day').contains(targetDay).click()

    // Assert that the input field contains the selected date
    const expectedDate = `${targetYear}-${String(targetDate.getMonth() + 1).padStart(2, '0')}-${String(targetDay).padStart(2, '0')}`
    cy.get('input#datepicker').should('have.value', expectedDate)
  })

    
    })
