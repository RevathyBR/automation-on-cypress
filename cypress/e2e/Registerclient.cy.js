describe('register', () => {

  
it('Invalid register client', () => {


        cy.visit ("https://dev.app.easyadvo.devateam.com/authentication/client-registration")
        //cy.title().should('eq', 'Nutzer Anmelden|EasyAdvo')

        cy.get("#fname").type("Mo")
        cy.get("#lname").type("T")

        cy.get("#email").type("mohind@gmail.com")
        cy.get('#password1').type("12345678")
        cy.get("#password2").type("12345678")
        cy.get("#gridCheck").check()

        cy.get('.btn').click()
       cy.wait(1000);

      // cy.get('.btn-primary-blue-outline').click()      //cookies decline
      cy.contains('Der Vorname muss mindestens 3 Zeichen lang sein').should('exist') //first name validation
      cy.contains('Der Nachname muss mindestens 3 Zeichen lang sein').should('exist') //second name validation

       cy.contains('Passwort muss enthalten::').should('exist')    //password validation

       cy.get("#password1").clear()
       cy.get('#password2').clear()
       cy.get("#fname").clear()
       cy.get("#lname").clear()

       cy.get("#fname").type("Mohind")
       cy.get("#lname").type("Test")
       cy.get('#password1').type("Mohind@123")
        cy.get("#password2").type("Mohind@123")



       cy.contains('Registrieren').should('exist');
       
       cy.get('.btn').click()
       cy.wait(1000);

       cy.contains('Ein Account mit dieser E-Mail existiert bereits').should('exist')
      
    })

      
it('register client', () => {


    cy.visit ("https://dev.app.easyadvo.devateam.com/authentication/client-registration")
    //cy.title().should('eq', 'Nutzer Anmelden|EasyAdvo')

    cy.get("#fname").type("Mohind")
    cy.get("#lname").type("Test")

    cy.get("#email").type("mohind@gmail.com")
    cy.get('#password1').type("12345678")
    cy.get("#password1").type("Mohind@12345")
    cy.get("#password2").type("Mohind@12345")
    cy.get("#gridCheck").check()

   cy.get('.btn-primary-blue-outline').click()      //cookies decline

   cy.contains('Registrieren').should('exist');
   
   cy.get('.btn').click()
   cy.wait(1000);

   cy.get('.success-page')
  cy.get ('.content-wrapper > .d-flex')
  cy.get('.text-center.text-16')

  cy.wait(1000);
 //cy.get('.text-blue').click()

  
})
})