describe('Data table', () => {

    beforeEach(() => {
    cy.visit ('https://athlete.devateam.com/login')
            cy.get('#email').type('superadmin@gmail.com')
            cy.get('#password').type('athlet_admin@123')
            cy.get('.btn-block').click()
            cy.wait(1000)
    })
    
  
    it('Dat table', () => {
    
    

            cy.visit('https://athlete.devateam.com/home')
            cy.visit('https://athlete.devateam.com/manage-students')
            cy.get('label > input').type('test')


            cy.get('#datatable-buttons_wrapper').within(() => {
                // Iterate through each row in the table body
                cy.get('tbody tr').each(($row) => {
                  // Check if the row contains the text "test"
                  cy.wrap($row).should(($rowContent) => {
                    // Assertion to check if "test" is found in the row
                    if ($rowContent.text().includes('test')) {
                      expect($rowContent.text()).to.include('test');
                    }
                  });
                });
              });
              cy.get('label > input').clear('test')
              cy.get('label > input').type('ios')
              cy.screenshot('searchpage')
              
            
              cy.contains('userios@mail.com').should('exist')

              cy.wait(1000)

             // it('Approve and decline staus', () => {
                cy.visit("https://athlete.devateam.com/enrollment-details")
                cy.get('label > input').type('Approved')

            cy.wait(1000)
            
            cy.get('#datatable-buttons_wrapper').within(() => {
                
                cy.get('tbody tr').each(($row) => {
                  
                  cy.wrap($row).should(($rowContent) => {
                
                    if ($rowContent.text().includes('Approved')) {
                      expect($rowContent.text()).to.include('Approved');
                    }
                  });
                });
              });
            /*const pagination = (initialContent) => {


                cy.get('[data-dt-idx="2"]').click();
                cy.wait(1000); 
              
                getTableContent().then(newContent => {
                  expect(newContent).not.to.deep.equal(initialContent, 'Table content should change after clicking next');
                  pagination(newContent)
                });*/
                cy.get('label > input').clear()
           cy.get('label > input').type('Pending')

            cy.wait(1000)
            
            cy.get('#datatable-buttons_wrapper').within(() => {
                
                cy.get('tbody tr').each(($row) => {
                  
                  cy.wrap($row).should(($rowContent) => {
                
                    if ($rowContent.text().includes('Pending')) {
                      expect($rowContent.text()).to.include('Pending');
                    }
                  });
                });
              });
              
    })

  })

