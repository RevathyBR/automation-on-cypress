describe('Data Table', () => {

beforeEach(() => {
  
        cy.visit ("https://dev.app.easyadvo.devateam.com/authentication/login")
       // cy.title().should('eq','EasyAdvo')
    cy.get(".mat-mdc-card-actions > .btn-primary-blue").click()             // cookies accept

    cy.contains('Anmelden').should('exist');  
    
        cy.get(":nth-child(1) > .form-control").type("mohind@gmail.com")
        cy.get(".password > .form-control").type("Mohind@123")
        cy.get('.btn[type="submit"]').click()
        cy.wait(3000);

    })
    


        it('Data Table on advisor search',() => {


            cy.visit("https://dev.app.easyadvo.devateam.com/client/home/advisor-search")

         cy.contains('Finde deinen Anwalt').should('exist')
 
         cy.contains('Suchergebnisse').should ('exist')
 
            cy.get(".mat-mdc-card-actions > .btn-primary-blue").click()            // cookies accept
         cy.wait(1000)
         
         cy.get('.d-flex > .location').select('Berlin')
         cy.get('[aria-label="topic"]').select('Family law')
 
         cy.get('.search-btn').click()
         cy.wait (1000);
 
         cy.get('.search-list-md > .search-list-item').should('be.visible');
         cy.wait(1000)
 
        cy.get('.search-list-md').scrollTo('bottom')
        cy.wait(1000)
 
        cy.get('.search-list-md').scrollTo('top')
        cy.wait(1000)

        cy.contains('Sortieren').should('exist')
        cy.get('.mb-3 > .d-flex > .mat-mdc-menu-trigger').click()
        //cy.contains('Name A biS Z').should('exist')
        cy.get(':nth-child(1) > .mdc-list-item__primary-text > .text-14').click()
        
        cy.wait(3000)

        cy.get('.mb-3 > .d-flex > .mat-mdc-menu-trigger').click()
       // cy.contains('Name Z bis A').should('exist')
        cy.get(':nth-child(2) > .mdc-list-item__primary-text > .text-14').click()
        cy.wait(3000)

        //cy.get(':nth-child(3) > .mdc-list-item__primary-text > .text-14').click()
       // cy.wait(3000)

       cy.get('.mb-3 > .d-flex > .mat-mdc-menu-trigger').click()
      // cy.contains('Gebhur absteigend').should('exist')
        cy.get(':nth-child(4) > .mdc-list-item__primary-text > .text-14').click()
        cy.wait(1000)


    })

})