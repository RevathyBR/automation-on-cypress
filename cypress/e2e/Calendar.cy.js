describe('Select Destination Test', () => {
    it('should select a destination', () => {
        cy.visit('https://www.emea.marriott.com/en');

        cy.get('#searchVM_Deststring').type('doha');
        cy.get('.searchResults.desktopPopularDestination').should('be.visible');

        // Click on the option for "Doha, Qatar" within the search results
        cy.contains('.setValueToInput', 'Doha, Qatar').click();
        cy.get('.next').click();
        cy.get('.right > .calendar-table > .table-condensed > tbody > :nth-child(2) > [data-title="r1c1"]').click();
        cy.get('.right > .calendar-table > .table-condensed > tbody > :nth-child(2) > [data-title="r1c5"]').click();
       // cy.get('.left > .calendar-table > .table-condensed > tbody > :nth-child(5) > [data-title="r4c0"]').click();
        //cy.get('.left > .calendar-table > .table-condensed > tbody > :nth-child(5) > [data-title="r4c5"]').click();
        cy.get('#btnHomeSearch_en').click();
    });
});